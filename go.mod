module gnet-hello-world

go 1.22.1

require (
	github.com/evanphx/wildcat v0.0.0-20141114174135-e7012f664567 // indirect
	github.com/panjf2000/gnet v1.6.7 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/vektra/errors v0.0.0-20140903201135-c64d83aba85a // indirect
	go.uber.org/atomic v1.11.0 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.27.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	gopkg.in/natefinch/lumberjack.v2 v2.2.1 // indirect
)
